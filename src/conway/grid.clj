(ns conway.grid
  (:refer-clojure :exclude [get]))

(defn point [x y]
  { :x x :y y })

(defn- within-bounds? [dimensions point]
  (let [{:keys [width height]} dimensions
        {:keys [x y]}          point]
    (and (>= x 1) (>= y 1) (<= x width) (<= y height))))

(defn within-grid? [grid point]
  (let [{:keys [dimensions]} grid]
    (within-bounds? dimensions point)))

(defn project [point dimensions]
  (let [{:keys [x y]}          point
        {:keys [width height]} dimensions]
    (if (not (within-bounds? dimensions point))
      (throw (IllegalArgumentException. "Point not within bounds"))
      (+ (* (dec y) width) (dec x)))))

(defn get [grid location]
  (let [{:keys [values dimensions]} grid]
    (values (project location dimensions))))

(defn neighbors [grid location]
  (let [{:keys [x y]} location
        left          (dec x)
        right         (inc x)
        up            (dec y)
        down          (inc y)]
    (into #{}
          (filter (partial within-grid? grid)
                  [(point left up)   (point x up)   (point right up)
                   (point left y)                   (point right y)
                   (point left down) (point x down) (point right down)]))))
