(ns conway.core
  (:require [conway.grid :as grid])
  (:gen-class))

(def ^:const survival-range #{2 3})

(def ^:const reproduction-threshold 3)

(defn- survives-among? [live-neighbors]
  (survival-range live-neighbors))

(defn- reproduces-among? [live-neighbors]
  (= live-neighbors reproduction-threshold))

(defn next-gen [current-state live-neighbors]
  (case current-state

    :alive
    (if (survives-among? live-neighbors)
      :alive :dead)

    :dead
    (if (reproduces-among? live-neighbors)
      :alive :dead)))

(defn current-state [world location]
  (grid/get world location))

(defn living-neighbors [world cell]
  (->> (grid/neighbors world cell)
      (map (partial current-state world))
      frequencies
      :alive))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
