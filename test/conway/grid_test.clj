(ns conway.grid-test
  (:require [clojure.test :refer :all]
            [conway.grid :refer :all])
  (:refer-clojure :exclude [get]))

(deftest project-point

  (testing "Project 2D point onto 3x3 1D world"
    (let [dimensions { :width 3 :height 3 }]

      (testing "Valid locations within space"
        (is (= (project (point 1 1) dimensions) 0))
        (is (= (project (point 2 1) dimensions) 1))
        (is (= (project (point 3 1) dimensions) 2))
        (is (= (project (point 1 2) dimensions) 3))
        (is (= (project (point 2 2) dimensions) 4))
        (is (= (project (point 3 2) dimensions) 5))
        (is (= (project (point 1 3) dimensions) 6))
        (is (= (project (point 2 3) dimensions) 7))
        (is (= (project (point 3 3) dimensions) 8)))

      (testing "Boundary checks for 3x3 space"
        (is (thrown-with-msg? IllegalArgumentException
                              #"Point not within bounds"
                              (project (point 0 0) dimensions)))))))

(deftest grid-get

  (testing "Query world locations for state"
    (let [grid { :values     [ :a :b :c
                               :d :e :f
                               :g :h :i ]
                 :dimensions { :width 3 :height 3}}]

      (is (= (get grid (point 1 1)) :a))
      (is (= (get grid (point 3 3)) :i)))))

(deftest within-grid
  (testing "Check if point is within valid bounds of the grid"
    (let [grid { :values     [ :a :b :c
                               :d :e :f
                               :g :h :i ]
                :dimensions { :width 3 :height 3}}]
      (is (= (within-grid? grid (point 2 2)) true))
      (is (= (within-grid? grid (point 4 1)) false))
      (is (= (within-grid? grid (point 0 3)) false))
      (is (= (within-grid? grid (point 1 5)) false))
      (is (= (within-grid? grid (point 3 0)) false))
      (is (= (within-grid? grid (point 0 0)) false))
      (is (= (within-grid? grid (point 4 4)) false)))))

(deftest grid-neighbors

  (testing "Get the current state for neighboring cells"
    (let [grid { :values     [ :a :b :c
                               :d :e :f
                               :g :h :i ]
                 :dimensions { :width 3 :height 3}}]

      (testing "No boundaries"
        (is (= (neighbors grid (point 2 2))
               #{(point 1 1) (point 2 1) (point 3 1)
                 (point 1 2)             (point 3 2)
                 (point 1 3) (point 2 3) (point 3 3)})))

      (testing "Boundary points"
        (is (= (neighbors grid (point 1 1))
               #{            (point 1 2)
                 (point 2 1) (point 2 2)}))))))
