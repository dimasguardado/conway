(ns conway.core-test
  (:require [clojure.test :refer :all]
            [conway.core :refer :all]
            [conway.grid :as grid]))

(deftest next-gen-rules

  (testing "Any live cell with less than two live neighbors dies"
    (is (= (next-gen :alive 1) :dead)))

  (testing "Any live cell with two or three live neighbors lives"
    (is (= (next-gen :alive 3) :alive)))

  (testing "Any live cell with more than three live neighbors dies"
    (is (= (next-gen :alive 4) :dead)))

  (testing "Any dead cell with exactly three live neighbors respawns"
    (is (= (next-gen :dead 3) :alive)))

  (testing "Any other dead cells remain dead"
    (is (= (next-gen :dead 2) :dead))))

(deftest current-state-world

  (testing "Query world locations for state"
    (let [world { :values       [ :alive :dead  :dead
                                  :dead  :alive :dead
                                  :alive :alive :dead ]
                  :dimensions { :width 3 :height 3}}]

      (is (= (current-state world (grid/point 1 1)) :alive))
      (is (= (current-state world (grid/point 3 3)) :dead)))))

(deftest count-living-neighbors

  (testing "returns the count of all living neighbors for a cell in a world"
    (let [world { :values    [ :alive :dead  :dead
                               :dead  :alive :dead
                               :alive :alive :dead ]
                 :dimensions { :width 3 :height 3 }}]

      (is (= (living-neighbors world (grid/point 2 2)) 3))
      (is (= (living-neighbors world (grid/point 1 1)) 1))
      (is (= (living-neighbors world (grid/point 1 2)) 4))
      (is (= (living-neighbors world (grid/point 3 3)) 2)))))
