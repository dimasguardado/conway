(defproject conway "0.1.0-SNAPSHOT"
  :description "Conway's Game of Life"
  :dependencies [[org.clojure/clojure "1.6.0"]]
  :main ^:skip-aot conway.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
